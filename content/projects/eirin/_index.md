---
title: eirin

links:
  github: https://github.com/ttpcodes/eirin
status: hiatus
type: professional
update: currently taking a break from project involvement for personal reasons
---
cgi frontend for admission of current mit students to discord servers
